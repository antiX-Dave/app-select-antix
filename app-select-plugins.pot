# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-22 14:21+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: app-select.conf:13
msgid "Open in text editor"
msgstr ""

#: app-select.conf:14
msgid "View icon"
msgstr ""

#: app-select.conf:15
msgid "Open in add-desktop"
msgstr ""

#: app-select.conf:16
msgid "+ Send icon to the toolbar"
msgstr ""

#: app-select.conf:17
msgid "- Remove this icon from the toolbar (if it is there)"
msgstr ""

#: app-select.conf:18
msgid "+ Send app to Personal menu"
msgstr ""

#: app-select.conf:19
msgid "- Remove this app from Personal Menu (if it is there)"
msgstr ""

#: app-select.conf:20
msgid "+ Send icon to the (zzz) Desktop"
msgstr ""
