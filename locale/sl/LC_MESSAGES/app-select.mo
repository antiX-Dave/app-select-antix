��          �      �           	     '     ;      H     i  
   m     x     �     �     �  	   �     �     �     �     �     �  	   �     �               "     7  �  I          %     5  )   G     q  
   u     �     �  
   �     �     �     �     �     �     �       	   
          +     ;     M     c                            
                                                                        	    
Displaying filtered results
   Display Columns    App Select  : could not run custom command
  All Categories Categories Only Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Name Name Only Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2023
Language-Team: Slovenian (https://www.transifex.com/anticapitalista/teams/10162/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 
Prikaz filtriranih rezultatov
 Prikaz stolpcev Izbira aplikacije : ukaza po meri ni bilo mogoče izvesti
  Vse Kategorije Zgolj kategorije Opis Zgolj opis Izvršna Zgolj izvršna Posodobitev ni bila uspešna Generično ime Zgolj generično ime Informacije Ime Zgolj ime Ponovno naloži seznam Zaženi program Iskanje / filter: Uspešno posodobljeno Tipkajte za filter... 