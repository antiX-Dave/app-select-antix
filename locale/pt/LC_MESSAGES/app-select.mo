��          �      �           	     '     ;      H     i  
   m     x     �     �     �  	   �     �     �     �     �     �  	   �     �               "     7  �  I  #        ,     >  7   X     �  
   �     �     �     �     �     �     �               )     6     ;     G     X     j     �     �                            
                                                                        	    
Displaying filtered results
   Display Columns    App Select  : could not run custom command
  All Categories Categories Only Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Name Name Only Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: Hugo Carvalho <hugokarvalho@hotmail.com>, 2023
Language-Team: Portuguese (https://www.transifex.com/anticapitalista/teams/10162/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 
A mostrar resultados da filtragem
 Mostrar colunas   Seleção de aplicações : não foi possível executar o comando personalizado
  Todos Categorias Apenas categorias Descrição Apenas descrição Executável Apenas executável Falha ao atualizar Nome genérico Apenas nome genérico Informação Nome Apenas nome Recarregar lista Executar programa Pesquisar / Filtrar:  Atualizado com êxito Escreva para filtrar... 