��          �      �           	     '     ;      H     i  
   m     x     �     �     �  	   �     �     �     �     �     �  	   �     �               "     7  �  I  $        :  .   P  7        �  
   �     �     �     �            "   &     I     X     s     �     �     �     �     �  )   �     �                            
                                                                        	    
Displaying filtered results
   Display Columns    App Select  : could not run custom command
  All Categories Categories Only Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Name Name Only Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2023
Language-Team: Portuguese (Brazil) (https://www.transifex.com/anticapitalista/teams/10162/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 
Exibindo os resultados da pesquisa
   Exibir em Colunas    Pesquisar Aplicativos com o ‘App Select’  : não foi possível executar o comando personalizado
  Todos Categorias Apenas pela Categoria Descrição Apenas pela Descrição Executável Apenas pelo Executável Ocorreu uma falha na atualização Nome Genérico Apenas pelo Nome Genérico Informações Nome Apenas pelo Nome Recarregar a Lista Executar o Programa Pesquisar / Filtrar:  A atualização foi realizada com sucesso Digite aqui para pesquisar... 