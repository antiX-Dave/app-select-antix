��          �      �           	     '     ;      H     i  
   m     x     �     �     �  	   �     �     �     �     �     �  	   �     �               "     7  �  I  J   �       !   =  ^   _     �     �     �     �          )     .  $   ?     d      |     �  
   �     �  %   �  )   �  "   #  #   F  @   j                            
                                                                        	    
Displaying filtered results
   Display Columns    App Select  : could not run custom command
  All Categories Categories Only Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Name Name Only Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: anticapitalista <anticapitalista@riseup.net>, 2023
Language-Team: Greek (https://www.transifex.com/anticapitalista/teams/10162/el/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: el
Plural-Forms: nplurals=2; plural=(n != 1);
 
Εμφάνιση φιλτραρισμένων αποτελεσμάτων
 Εμφάνιση στηλών Επιλογή εφαρμογής : δεν ήταν δυνατή η εκτέλεση προσαρμοσμένης εντολής Όλες Κατηγορίες Μόνο Κατηγορίες Περιγραφή Μόνο Περιγραφή Exec  
Μόνο Exec Η ενημέρωση απέτυχε Γενικό Όνομα Μόνο γενικό όνομα Πληροφορίες Όνομα Μόνο όνομα Επαναφόρτωση λίστας Εκτέλεση προγράμματος Αναζήτηση / Φίλτρο: επιτυχής ενημέρωση Πληκτρολογήστε για να φιλτράρετε... 