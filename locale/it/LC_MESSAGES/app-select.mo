��          �      �           	     '     ;      H     i  
   m     x     �     �     �  	   �     �     �     �     �     �  	   �     �               "     7  �  I  $   �          ,  -   :     h  	   n     x     �     �  
   �     �     �     �     �     �     �  	                  ,     :     R                            
                                                                        	    
Displaying filtered results
   Display Columns    App Select  : could not run custom command
  All Categories Categories Only Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Name Name Only Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: Dede Carli <dede.carli.drums@gmail.com>, 2023
Language-Team: Italian (https://www.transifex.com/anticapitalista/teams/10162/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 
Visualizzazione risultati filtrati
 Mostra colonne Seleziona app : non posso eseguire comandi personalizzati
  Tutti Categorie Solo categorie Descrizione Solo descrizione Eseguibile Solo eseguibile Impossibile aggiornare Nome generico Solo nome generico Info Nome Solo nome Ricarica elenco Esegui programma Cerca/filtra: Aggiornato con successo Digita per filtrare... 