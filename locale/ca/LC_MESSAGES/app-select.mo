��          �      �           	     '     ;      H     i  
   m     x     �     �     �  	   �     �     �     �     �     �  	   �     �               "     7  ~  I      �     �     �  #        /     5     A     S     `     s     x     �     �     �     �     �  
   �     �     �          !     @                            
                                                                        	    
Displaying filtered results
   Display Columns    App Select  : could not run custom command
  All Categories Categories Only Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Name Name Only Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: Eduard Selma <selma@tinet.cat>, 2023
Language-Team: Catalan (https://www.transifex.com/anticapitalista/teams/10162/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 
Mostra dels resultats filtrats
 Mostra columnes Tria l'aplicació no puc executar l'ordre específica Tots  Categories  Només categories Descripció  Només descripció Exec Només executable Ha fallat l'actualització Nom genèric Només nom genèric Informació Nom  Només nom Torna a carregar la llista Executa el programa Cerca / Filtre: Actualització satisfactòria  Tipus a filtrar... 