��          �      �           	     '     ;      H     i  
   m     x     �     �     �  	   �     �     �     �     �     �  	   �     �               "     7  �  I     �     �     �  '        3  
   8     C     R     ^     n     w     �     �     �     �     �     �     �     �     �                                      
                                                                        	    
Displaying filtered results
   Display Columns    App Select  : could not run custom command
  All Categories Categories Only Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Name Name Only Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2023
Language-Team: Norwegian Bokmål (https://www.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 
Viser filtrerte resultater
 Vis kolonner Velg program : klarte ikke kjøre selvvalgt kommando Alle Kategorier Kun kategorier Beskrivelse Kun beskrivelse Kjørbar Kun kjørbare Klarte ikke oppdatere Generisk navn Kun generisk navn Info Navn Kun navn Last inn liste på ny Kjør program Søk / filtrer: Oppdatering fullført Skriv for å filtrere … 