��          �      �           	     '     ;      H     i  
   m     x     �     �     �  	   �     �     �     �     �     �  	   �     �               "     7  �  I     �     �  	   �  &        )  
   .     9     K     W     j     o     {     �     �     �     �     �     �     �     �     �                                 
                                                                        	    
Displaying filtered results
   Display Columns    App Select  : could not run custom command
  All Categories Categories Only Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Name Name Only Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: Henry Oquist <henryoquist@nomalm.se>, 2023
Language-Team: Swedish (https://www.transifex.com/anticapitalista/teams/10162/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
 
Visar filtrerade resultat
 Visa Kolumner Välj App : kunde inte köra anpassat kommando
  Alla Kategorier Enbart Kategorier Beskrivning Enbart Beskrivning Exec Enbart Exec Misslyckades med uppdatering Allmänt Namn Enbart Allmänt Namn Info Namn Enbart Namn Ladda om Lista Kör Program Sök / Filter:  Lyckad uppdatering Typ att filtrera... 