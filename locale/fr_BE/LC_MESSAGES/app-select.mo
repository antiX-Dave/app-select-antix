��          �      �           	     '     ;      H     i  
   m     x     �     �     �  	   �     �     �     �     �     �  	   �     �               "     7  �  I  #   �       '   7  7   _     �     �     �     �     �     �     �          *     :     W     \     `     q     �     �     �     �                            
                                                                        	    
Displaying filtered results
   Display Columns    App Select  : could not run custom command
  All Categories Categories Only Description Description Only Exec Exec Only Failed to update Generic Name Generic Name Only Info Name Name Only Reload List Run Program Search / Filter:  Successfully updated Type to filter... Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 14:20+0000
Last-Translator: Wallon Wallon, 2023
Language-Team: French (Belgium) (https://app.transifex.com/anticapitalista/teams/10162/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 
Affichage des résultats filtrés
   Afficher les colonnes    Chercher une application - App Select  : impossible d’exécuter la commande personnalisée
  Tous  Catégories Seulement les catégories Description Seulement la description Exécutable Seulement les exécutables La mise à jour a échoué Nom générique Seulement le nom générique Info Nom Seulement le nom Recharger la liste Lancer le programme Rechercher / Filtrer:  Mise à jour effectuée Tapez pour filtrer ... 